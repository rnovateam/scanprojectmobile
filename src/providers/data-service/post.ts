export interface Post {

  'nombreequipo': string;
  'descripcioncondi': string;
  'fecha': string;
  'nombrecompeticion': string;
  'posesion': string;
  'goles': string;
  'penales': string;
  'remates': string;
  'remates_puerta': string;
  'corners': string;
  'faltas': string;
  'tarjetas_rojas': string;
  'tarjetas_amarillas': string;
  'fueras_juego': string;
}

