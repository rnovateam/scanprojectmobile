import { Injectable } from '@angular/core';
import { Http, Response,RequestOptions, Headers  } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import 'rxjs/add/operator/map';
import 'rxjs/operator/delay';
import 'rxjs/operator/mergeMap';
import 'rxjs/operator/switchMap';
import { map } from 'rxjs/operators';

/*
  Generated class for the DataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()

export class DataServiceProvider {
  serverBaseUrl="http://c1450068.ferozo.com/backend/web/index.php?r=";
  serverImgUrl="http://c1450068.ferozo.com/backend/web/uploads/productos/";
  data;
  constructor(public http: Http) {
  }
  // getData(id) {
  //   var url = this.serverBaseUrl + "api/producto/viewproducto";
  //   var headers = new Headers({
  //     'Content-Type': 'application/x-www-form-urlencoded'
  //   })
  //   var formdata = "'&id="+id+"";
  //   var ops = new RequestOptions({ headers: headers });
  //   return this.http.post(url, JSON.stringify(formdata),ops);
  // }
  sendPedidoMedidor(id) {
    var url = this.serverBaseUrl + "api/producto/viewproducto";
    var headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
    var ops = new RequestOptions({headers:headers});
    var formdata = "'&id="+id+"";
    return new Promise((resolve, reject)=>{
      this.http.post(encodeURI(url),formdata,ops).map((res:Response) => res.json())
      .subscribe(data=>{
        this.data = data;
        console.log(this.data);
        resolve(this.data);
      },err=>reject(err));
    })
  }
}
