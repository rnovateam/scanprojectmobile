import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { DataServiceProvider } from '../../providers/data-service/data-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  valueProduct: any;
  pageImage = this.dataservice.serverImgUrl;
  imagen;
  nombre;
  stock;
  precio_venta;
  marca;
  descripcion;
  constructor(private dataservice: DataServiceProvider, public navCtrl: NavController,private barcodeScanner: BarcodeScanner ) {
  }
  scan(){
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);
      this.getProduct(barcodeData.text);
     }).catch(err => {
         console.log('Error', err);
     });

  }
  getProduct(id){
    // console.log(id);
    // this.dataservice.getData(id).subscribe(res => {
    //   console.log(res)
    //   this.valueProduct = res;
    //   console.log(this.valueProduct.name);
    //    })

       this.dataservice.sendPedidoMedidor(id).then((res)=> {
               this.valueProduct = res;
               console.log(this.valueProduct.nombre);
              this.imagen = this.pageImage + this.valueProduct.imagen;
              this.nombre = this.valueProduct.nombre;
              this.marca = this.valueProduct.marca;
              this.stock = this.valueProduct.stock;
              this.descripcion = this.valueProduct.descripcion;
              this.precio_venta = this.valueProduct.precio_venta;
             },(err)=>{
             });


  }
  resetProduct(){
    this.valueProduct = '';
  }

}
