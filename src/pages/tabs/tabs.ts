import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { Platform } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  isAndroid: boolean = false;


  tab1Root = HomePage;
  // tab2Root = AboutPage;
  tab3Root = ContactPage;

  constructor(platform: Platform) {
    this.isAndroid = platform.is('android');

    console.log("entre al modulo tabs");
  }
}
